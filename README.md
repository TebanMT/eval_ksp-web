# WebKsp

## Description :book:
En este repositorio se encuentra el SITIO WEB, desrrollado con angular, para la evaluación tencnica de la empresa KSP.


## Tecnologias Utilizadas
- node
- angular   -- **web framework**
- boostrap  -- **Framework de Estilo** 
- scss    -- **preprocesaro de hojas de estilo**
- html5  -- **maquetado**

## Instalación :hammer:
### Desarrollo

Este proyecto no cuenta con un Makefile, sin embargo, levantarlo en modo desarrollador no debe ser tan complicado.
Primero se requiere tener angular-cli.
Ejecutar `npm start` para iniciar el servidor. Navegar a `http://localhost:4200/`

En caso de requerir iniciar con una instancia de docker en modo produccion, existe el archivo doker-compose.yml que lo ayudara:

```
$ docker-compose build
$ docker-compose up     // Levantar los servicios y verificar logs
$ docker-compose up -d  // Modo detach (remplazo del comando anterior si no desea ver logs)

// La app la expondra nginx en el puerto 4200
```

### Producción

Actualmente se encuentra ejecutandose una instancia de la app en un drop de DigitalOcean: http://165.232.132.62:4200/Home


Inicialmente se desplegó en App Platform de DigitalOcean, la version PaaS de Digital. Sin embargo al estar la api sin un certificado ssl, las peticiones no son posibles dado que se aloja en https. Esto cambiara cuando la api soporte https.
No obstante en la siguiente liga se puede ver que efectivamente tambien se encuentra en un servicio PaaS: https://plankton-app-76ygv.ondigitalocean.app/Home


## TODOS
- [ ] Pruebas Unitarias
- [ ] Agregar más de un beneficiario (el api ya lo soporta)
- [ ] Componente para editar beneficiario
- [ ] Modificar estilo de boton subir foto
- [x] Agregar validaciones a los form add employe
- [ ] Agregar validaciones a los form update employe
- [ ] Crear docker para desarrollo (con livereload)
- [ ] Cambiar readme a ingles
