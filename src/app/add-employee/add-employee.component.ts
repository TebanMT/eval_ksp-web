import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpProviderService } from '../Service/http-provider.service';

class ImageSnippet {
  constructor(public src: string, public file: File) {}
}

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.scss']
})
export class AddEmployeeComponent implements OnInit {
  addEmployeeForm: employeeForm = new employeeForm();
  addBenifiaryForm: benifiaryForm = new benifiaryForm();

  @ViewChild("employeeForm")
  employeeForm!: NgForm;
  @ViewChild("benifiaryForm")
  benifiaryForm!: NgForm;
  isSubmitted: boolean = false;
  selectedFile: ImageSnippet;
  status: any;
  constructor(private router: Router, private httpProvider: HttpProviderService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.status = ["Activo","In-Activo"];
  }

  ProcessFile(imageInput: any) {
    console.log(imageInput.files)
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.selectedFile = new ImageSnippet(event.target.result, file);
    });

    reader.readAsDataURL(file);
  }

  AddEmployee(isValid: any) {
    this.isSubmitted = true;
    if (isValid) {
      this.addEmployeeForm.photo=this.selectedFile.src
      this.httpProvider.saveEmployee(this.addEmployeeForm, this.addBenifiaryForm).subscribe(async data => {
        if (data != null && data.body != null) {
          if (data != null && data.body != null) {
            var resultData = data.body;
            console.log(data)
            if (resultData != null && data.status==201) {
              this.toastr.success(resultData.message);
              setTimeout(() => {
                this.router.navigate(['/Home']);
              }, 500);
            }
          }
        }
      },
        async error => {
          this.toastr.error(error.message);
          setTimeout(() => {
            this.router.navigate(['/Home']);
          }, 500);
        });
    }
  }
}

export class employeeForm {
  name: string = "";
  job_position: string = "";
  salary: string = "";
  status: string = "activo";
  date_hire: string = "";
  photo: any;
}

export class benifiaryForm {
  name: string = "";
  relationship: string = "";
  date_born: string = "";
  gender: string = "Mujer";
  employed_id: string="";
}
